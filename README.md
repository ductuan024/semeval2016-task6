# SemEval2016-Task6

Reproduce from paper (BCA model): https://arxiv.org/ftp/arxiv/papers/2010/2010.05471.pdf


C++ code: https://github.com/Luoyufeichen/Dgnn-BiCond

Reference for DANN (Domain Adaptation Neural Net): https://github.com/fungtion/DANN (The BCA use this structure, the only different is feature extractor part)
